namespace Bloghaha.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        PostID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 50),
                        Body = c.String(nullable: false),
                        DayCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.PostID);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Body = c.String(nullable: false, maxLength: 100),
                        DayCreated = c.DateTime(nullable: false),
                        PostID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .Index(t => t.PostID);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagID = c.Int(nullable: false, identity: true),
                        Content = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.TagID);
            
            CreateTable(
                "dbo.Tag_Post",
                c => new
                    {
                        PostID = c.Int(nullable: false),
                        TapID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostID, t.TapID })
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TapID, cascadeDelete: true)
                .Index(t => t.PostID)
                .Index(t => t.TapID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Tag_Post", new[] { "TapID" });
            DropIndex("dbo.Tag_Post", new[] { "PostID" });
            DropIndex("dbo.Comments", new[] { "PostID" });
            DropForeignKey("dbo.Tag_Post", "TapID", "dbo.Tags");
            DropForeignKey("dbo.Tag_Post", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Comments", "PostID", "dbo.Posts");
            DropTable("dbo.Tag_Post");
            DropTable("dbo.Tags");
            DropTable("dbo.Comments");
            DropTable("dbo.Posts");
        }
    }
}
