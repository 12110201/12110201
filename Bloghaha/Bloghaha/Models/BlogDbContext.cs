﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Bloghaha.Models
{
    public class BlogDbContext:DbContext
    {
        public DbSet<Post> Post { set; get; }
        public DbSet<Comment> Comment { set; get; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Post>()
         .HasMany(d => d.Tags).WithMany(p =>p.Posts)
            .Map(t => t.MapLeftKey("PostID").MapRightKey("TapID").ToTable ("Tag_Post"));
        }

        public DbSet<Tag> Tags { get; set; }
    }
}