﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Bloghaha.Models
{
    public class Comment
    {
        public int ID { set; get; }
        [Required]
        [StringLength(100,ErrorMessage="Ký tự trong khoảng 10->100",MinimumLength=10)]
        public String Body { set; get; }
        [Required]
       [DataType(DataType.DateTime)]
        public DateTime DayCreated { set; get; }
        
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}