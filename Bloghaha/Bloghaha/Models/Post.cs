﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Bloghaha.Models
{
    public class Post
    {
        public int PostID { set; get; }
        [Required]
        [StringLength(50,ErrorMessage="Ký tự nằm trong khoảng 10->50",MinimumLength=10)]
        public String Title { set; get; }
        [Required]
        public String Body { set; get; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DayCreated { set; get; }
       
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}