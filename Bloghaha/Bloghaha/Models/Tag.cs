﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Bloghaha.Models
{
    public class Tag
    {
        public int TagID { set; get; }
        [Required]
        [StringLength(500,ErrorMessage="Kí tự trong khoảng 50->500",MinimumLength=50)]
        public String Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}