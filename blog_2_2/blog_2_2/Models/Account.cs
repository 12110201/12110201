﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace blog_2_2.Models
{
    public class Account
    {
        [DataType(DataType.Password)]
        public String Password { set; get; }
        [DataType(DataType.EmailAddress)]
        public String Email { set; get; }
        public String FirstName { set; get; }
        public String LastName { set; get; }
        
        public int AccountID { set; get; }

        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}