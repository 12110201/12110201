namespace blog_2_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan6 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountID = c.Int(nullable: false, identity: true),
                        Password = c.String(),
                        Email = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        PostID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AccountID)
                .ForeignKey("dbo.BaiViet", t => t.PostID, cascadeDelete: true)
                .Index(t => t.PostID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Accounts", new[] { "PostID" });
            DropForeignKey("dbo.Accounts", "PostID", "dbo.BaiViet");
            DropTable("dbo.Accounts");
        }
    }
}
