﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace webit.Models
{
    public class Assgement
    {
        [Key]
        public int AssgementID { set; get; }
        public String DateAss { set; get; }
        public String Content { set; get; }
        public String Type { set; get; }
        public int ScheduleID { set; get; }
        public int Score { set; get; }
        public virtual Schedule Schedule { set; get; }
    }
}