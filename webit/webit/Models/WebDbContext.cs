﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace webit.Models
{
    public class WebDbContext: DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<User>()
                .HasOptional(e => e.Partner)
                .WithMany();
        }
        public DbSet<Schedule> Schedule { set; get; }
        public DbSet<MissingList> MissingList { set; get; }
        public DbSet<Assgement> Assgement { set; get; }
        public DbSet<Communication> Communication { set; get; }

        public DbSet<User> Users { get; set; }
    }
}