﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace webit.Models
{
    public class User
    {
        [Key]
        [Required]
        public int UserID { set;get; }
        public String UserName { set; get; }
       [Required]
        [DataType(DataType.Password)]
       public String Password { set; get; }
        public String Address { set; get; }
        [Required]
        public String Phone { set; get; }
        [DataType(DataType.EmailAddress)]
        public String Email { set; get; }
        [Required]
        [StringLength(500,ErrorMessage="ban phai nhap tu 10 ky tu tro len",MinimumLength=10)]
        public String Purpose { set; get; }
        [Required]
        public int TermTimeLeft { set; get; }

        public String PName { set; get; }
       
        public virtual ICollection< Schedule >Schedule { set; get; }
        public virtual User Partner { set; get; }
    }
}