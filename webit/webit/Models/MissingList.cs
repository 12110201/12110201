﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace webit.Models
{
    public class MissingList
    {
        [Key]
        public int MissingID { set; get; }
        public String Day { set; get; }

        public String Job { set; get; }
   

        public String DateJob { set; get; }
        // public Boolean Checked { set; get; }
        public int ScheduleID { set; get; }
        public virtual Schedule Schedule { set; get; }
    }
}