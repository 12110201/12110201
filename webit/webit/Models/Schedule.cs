﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace webit.Models
{
    public class Schedule
    {

        [Key]
        public int ScheduleID { set; get; }
        [Required]
        public String ScheduleName { set; get; }
        public String Day { set; get; }

        public String Job { set; get; }

        public String DateJob { set; get; }
        public Boolean Checked { set; get; }
        public int UserID { set; get; }
        public virtual User User { set; get; }
        public virtual ICollection<MissingList> MissingLists { set; get; }
        public virtual ICollection<Assgement> Assgements { set; get; }
        public virtual ICollection<Communication> Communications { set; get; }
    }
}