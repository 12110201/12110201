﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace webit.Models
{
    public class Communication
    {
        [Key]
        public int ComID { set; get; }
        public DateTime DateJ { set; get; }
        public String Title { set; get; }
        public String Job { set; get; }
        public int ScheduleID { set; get; }
        public virtual Schedule Schedule { set; get; }
    }
}