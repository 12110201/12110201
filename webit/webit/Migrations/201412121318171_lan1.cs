namespace webit.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Schedules",
                c => new
                    {
                        ScheduleID = c.Int(nullable: false, identity: true),
                        ScheduleName = c.String(nullable: false),
                        Day = c.String(),
                        Job = c.String(),
                        DateJob = c.String(),
                        Checked = c.Boolean(nullable: false),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ScheduleID)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        Password = c.String(nullable: false),
                        Address = c.String(),
                        Phone = c.String(nullable: false),
                        Email = c.String(),
                        Purpose = c.String(nullable: false, maxLength: 500),
                        TermTimeLeft = c.Int(nullable: false),
                        PName = c.String(),
                        Partner_UserID = c.Int(),
                    })
                .PrimaryKey(t => t.UserID)
                .ForeignKey("dbo.Users", t => t.Partner_UserID)
                .Index(t => t.Partner_UserID);
            
            CreateTable(
                "dbo.MissingLists",
                c => new
                    {
                        MissingID = c.Int(nullable: false, identity: true),
                        Day = c.String(),
                        Job = c.String(),
                        DateJob = c.String(),
                        ScheduleID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MissingID)
                .ForeignKey("dbo.Schedules", t => t.ScheduleID, cascadeDelete: true)
                .Index(t => t.ScheduleID);
            
            CreateTable(
                "dbo.Assgements",
                c => new
                    {
                        AssgementID = c.Int(nullable: false, identity: true),
                        DateAss = c.String(),
                        Content = c.String(),
                        Type = c.String(),
                        ScheduleID = c.Int(nullable: false),
                        Score = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AssgementID)
                .ForeignKey("dbo.Schedules", t => t.ScheduleID, cascadeDelete: true)
                .Index(t => t.ScheduleID);
            
            CreateTable(
                "dbo.Communications",
                c => new
                    {
                        ComID = c.Int(nullable: false, identity: true),
                        DateJ = c.DateTime(nullable: false),
                        Title = c.String(),
                        Job = c.String(),
                        ScheduleID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ComID)
                .ForeignKey("dbo.Schedules", t => t.ScheduleID, cascadeDelete: true)
                .Index(t => t.ScheduleID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Communications", new[] { "ScheduleID" });
            DropIndex("dbo.Assgements", new[] { "ScheduleID" });
            DropIndex("dbo.MissingLists", new[] { "ScheduleID" });
            DropIndex("dbo.Users", new[] { "Partner_UserID" });
            DropIndex("dbo.Schedules", new[] { "UserID" });
            DropForeignKey("dbo.Communications", "ScheduleID", "dbo.Schedules");
            DropForeignKey("dbo.Assgements", "ScheduleID", "dbo.Schedules");
            DropForeignKey("dbo.MissingLists", "ScheduleID", "dbo.Schedules");
            DropForeignKey("dbo.Users", "Partner_UserID", "dbo.Users");
            DropForeignKey("dbo.Schedules", "UserID", "dbo.Users");
            DropTable("dbo.Communications");
            DropTable("dbo.Assgements");
            DropTable("dbo.MissingLists");
            DropTable("dbo.Users");
            DropTable("dbo.Schedules");
        }
    }
}
