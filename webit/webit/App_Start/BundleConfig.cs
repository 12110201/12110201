﻿using System.Web;
using System.Web.Optimization;

namespace webit
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/Scripts").Include("~/Scripts/jquery-1.11.1.min.js",
                        "~/Scripts/bootstrap.min.js",
                        "~/Scripts/TweenMax.min.js",
                        "~/Scripts/resizeable.js",
                        "~/Scripts/joinable.js",
                        "~/Scripts/xenon-api.js",
                        "~/Scripts/xenon-toggles.js",
                        "~/Scripts/jquery-validate/jquery.validate.min.js",
                        "~/Scripts/toastr/toastr.min.js",
                        "~/Scripts/xenon-custom.js",
                        "~/Scripts/inputmask/jquery.inputmask.bundle.js"));
            bundles.Add(new ScriptBundle("~/Scripts/datatables").Include("~/Scripts/datatables/js/jquery.dataTables.min.js",
                                                                        "~/Scripts/datatables/dataTables.bootstrap.js",
                                                                        "~/Scripts/datatables/yadcf/jquery.dataTables.yadcf.js",
                                                                        "~/Scripts/datatables/tabletools/dataTables.tableTools.min.js"));
            bundles.Add(new StyleBundle("~/Scripts/datatables/css").Include("~/Scripts/datatables/dataTables.bootstrap.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/bootstrap.css",
                                                                    "~/Content/custom.css",
                                                                    "~/Content/xenon-components.css",
                                                                    "~/Content/xenon-core.css",
                                                                    "~/Content/xenon-forms.css",
                                                                    "~/Content/xenon-skins.css",
                                                                    "~/Content/xenon.css"
                                                                    ));
            bundles.Add(new StyleBundle("~/Content/fonts/linecons/css/css").Include(
                                                                    "~/Content/fonts/linecons/css/linecons.css"));

            bundles.Add(new StyleBundle("~/Content/fonts/fontawesome/css/css").Include(
                                                                    "~/Content/fonts/fontawesome/css/font-awesome.min.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));
        }
    }
}