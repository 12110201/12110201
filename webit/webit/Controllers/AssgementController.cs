﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webit.Models;

namespace webit.Controllers
{
    public class AssgementController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /Assgement/

        public ActionResult Index()
        {
            var assgement = db.Assgement.Include(a => a.Schedule);
            return View(assgement.ToList());
        }

        //
        // GET: /Assgement/Details/5

        public ActionResult Details(int id = 0)
        {
            Assgement assgement = db.Assgement.Find(id);
            if (assgement == null)
            {
                return HttpNotFound();
            }
            return View(assgement);
        }

        //
        // GET: /Assgement/Create

        public ActionResult Create()
        {
            ViewBag.ScheduleID = new SelectList(db.Schedule, "ScheduleID", "ScheduleName");
            return View();
        }

        //
        // POST: /Assgement/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Assgement assgement)
        {
            if (ModelState.IsValid)
            {
                db.Assgement.Add(assgement);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ScheduleID = new SelectList(db.Schedule, "ScheduleID", "ScheduleName", assgement.ScheduleID);
            return View(assgement);
        }

        //
        // GET: /Assgement/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Assgement assgement = db.Assgement.Find(id);
            if (assgement == null)
            {
                return HttpNotFound();
            }
            ViewBag.ScheduleID = new SelectList(db.Schedule, "ScheduleID", "ScheduleName", assgement.ScheduleID);
            return View(assgement);
        }

        //
        // POST: /Assgement/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Assgement assgement)
        {
            if (ModelState.IsValid)
            {
                db.Entry(assgement).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ScheduleID = new SelectList(db.Schedule, "ScheduleID", "ScheduleName", assgement.ScheduleID);
            return View(assgement);
        }

        //
        // GET: /Assgement/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Assgement assgement = db.Assgement.Find(id);
            if (assgement == null)
            {
                return HttpNotFound();
            }
            return View(assgement);
        }

        //
        // POST: /Assgement/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Assgement assgement = db.Assgement.Find(id);
            db.Assgement.Remove(assgement);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}