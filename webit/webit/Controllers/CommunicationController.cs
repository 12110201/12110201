﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webit.Models;

namespace webit.Controllers
{
    public class CommunicationController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /Communication/

        public ActionResult Index()
        {
            var communication = db.Communication.Include(c => c.Schedule);
            return View(communication.ToList());
        }

        //
        // GET: /Communication/Details/5

        public ActionResult Details(int id = 0)
        {
            Communication communication = db.Communication.Find(id);
            if (communication == null)
            {
                return HttpNotFound();
            }
            return View(communication);
        }

        //
        // GET: /Communication/Create

        public ActionResult Create()
        {
            ViewBag.ScheduleID = new SelectList(db.Schedule, "ScheduleID", "ScheduleName");
            return View();
        }

        //
        // POST: /Communication/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Communication communication)
        {
            if (ModelState.IsValid)
            {
                db.Communication.Add(communication);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ScheduleID = new SelectList(db.Schedule, "ScheduleID", "ScheduleName", communication.ScheduleID);
            return View(communication);
        }

        //
        // GET: /Communication/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Communication communication = db.Communication.Find(id);
            if (communication == null)
            {
                return HttpNotFound();
            }
            ViewBag.ScheduleID = new SelectList(db.Schedule, "ScheduleID", "ScheduleName", communication.ScheduleID);
            return View(communication);
        }

        //
        // POST: /Communication/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Communication communication)
        {
            if (ModelState.IsValid)
            {
                db.Entry(communication).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ScheduleID = new SelectList(db.Schedule, "ScheduleID", "ScheduleName", communication.ScheduleID);
            return View(communication);
        }

        //
        // GET: /Communication/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Communication communication = db.Communication.Find(id);
            if (communication == null)
            {
                return HttpNotFound();
            }
            return View(communication);
        }

        //
        // POST: /Communication/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Communication communication = db.Communication.Find(id);
            db.Communication.Remove(communication);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}