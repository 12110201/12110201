﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webit.Models;

namespace webit.Controllers
{
    public class MissingListController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /MissingList/

        public ActionResult Index()
        {
            var missinglist = db.MissingList.Include(m => m.Schedule);
            return View(missinglist.ToList());
        }

        //
        // GET: /MissingList/Details/5

        public ActionResult Details(int id = 0)
        {
            MissingList missinglist = db.MissingList.Find(id);
            if (missinglist == null)
            {
                return HttpNotFound();
            }
            return View(missinglist);
        }

        //
        // GET: /MissingList/Create

        public ActionResult Create()
        {
            ViewBag.ScheduleID = new SelectList(db.Schedule, "ScheduleID", "ScheduleName");
            return View();
        }

        //
        // POST: /MissingList/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MissingList missinglist)
        {
            if (ModelState.IsValid)
            {
                db.MissingList.Add(missinglist);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ScheduleID = new SelectList(db.Schedule, "ScheduleID", "ScheduleName", missinglist.ScheduleID);
            return View(missinglist);
        }

        //
        // GET: /MissingList/Edit/5

        public ActionResult Edit(int id = 0)
        {
            MissingList missinglist = db.MissingList.Find(id);
            if (missinglist == null)
            {
                return HttpNotFound();
            }
            ViewBag.ScheduleID = new SelectList(db.Schedule, "ScheduleID", "ScheduleName", missinglist.ScheduleID);
            return View(missinglist);
        }

        //
        // POST: /MissingList/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MissingList missinglist)
        {
            if (ModelState.IsValid)
            {
                db.Entry(missinglist).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ScheduleID = new SelectList(db.Schedule, "ScheduleID", "ScheduleName", missinglist.ScheduleID);
            return View(missinglist);
        }

        //
        // GET: /MissingList/Delete/5

        public ActionResult Delete(int id = 0)
        {
            MissingList missinglist = db.MissingList.Find(id);
            if (missinglist == null)
            {
                return HttpNotFound();
            }
            return View(missinglist);
        }

        //
        // POST: /MissingList/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MissingList missinglist = db.MissingList.Find(id);
            db.MissingList.Remove(missinglist);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}