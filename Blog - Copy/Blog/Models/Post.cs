﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Post
    {

        public int PostID { set; get; }
        [Required]
        [StringLength(500, ErrorMessage = "Title có 20 đến 500 ký tự", MinimumLength = 20)]
        public String Title { set; get; }
        [Required]
        [StringLength(1000, ErrorMessage = "Body có tối đa 50 ký tự", MinimumLength = 50)]
        public String Body { set; get; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        public int UserID { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        public virtual UserProfile UserProfile { set; get; }
    }
}